#!/bin/bash
#=================================
#   DESCRIPTION:log监控 
#       设备组:vfcc 
# 
#        AUTHOR: 韦启胜 
#       CREATED: 2017/3/6 
#================================
check_vfcc_reportAgent(){
       if [ -f /cache/logs/reportAgent/log/report.log ];then
        local sum_err=$(tail -n500 /cache/logs/reportAgent/log/report.log|grep "$minago"|grep  -E 'does not exist|Exception'|wc -l)
        return $sum_err
else
   exit 1
fi
}
##################################################################
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
minago=$(date -d "1 minutes ago" '+%Y-%m-%d %H:%M:')
vfcc=$(ps -ef|grep vfcc|grep nginx|wc -l)
endpoint=$(hostname -s)
timestamp=$(date +%s)

if [[ $vfcc -gt 0 ]]; then
    msg1=$(check_vfcc_reportAgent)
    value=$?
else
    exit;

fi
    echo -n [{\"endpoint\": \"$endpoint\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.vfcc.reportAgent.log\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 60}]
