#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

endpoint=`hostname -s`
value=`curl --connect-timeout 10 -m 10 -o /dev/null -s -w %{time_total} -H "Host: client03.pdl.wow.battlenet.com.cn"  "http://127.0.0.1:81/tpr/pro/data/cb/5e/cb5e91ad16022f80e5a4a84fe8f32b38.index" 2> /dev/null`
timestamp=`date +%s`

value=$(echo "$value*1000"|bc)

cat << EOF
    [{
        "endpoint"      :"$endpoint",
        "tags"          :"",
        "timestamp"     :$timestamp,
        "metric"        :"vfcc.squid.response.time",
        "value"         :$value,
        "counterType"   :"GAUGE",
        "step"          :60
    }]
EOF
