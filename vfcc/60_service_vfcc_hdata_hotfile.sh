#!/bin/bash 
#================================= 
#   DESCRIPTION:访问热点文件监控 
#       设备组:vfcc 
# 
#        AUTHOR: 韦启胜 
#       CREATED: 2017/2/17 
#update 修改了 判断全局缓存条件，加入wdata判断
#2017-2-28 update 增加进入热点条件，过滤小于2k文件
#================================

check_hotfile(){
    nowtime=$(date +%Y%m%d%H%M)
    ACC_LOG=$(ls /cache/logs/heka-cronolog/$nowtime/vfcc-*.client03.pdl.wow.battlenet.com.cn*.log)
    hotfile_sum=$(cat $ACC_LOG | awk '{print $7}'|sort|uniq -c|sort -rn|head -n3|awk '{print $1}')
   for hot in ${hotfile_sum[@]};do
      if [ $hot -gt 5 ];then
    		hoturl=$(cat $ACC_LOG |awk '$10 >= 2048 {print $7}'|sort|uniq -c|sort -rn|head -n3|awk '{print $2}' | cut -d "." -f 2-  |sed 's/^/\/\*data\/&/g')
	for url in ${hoturl[@]};do
		if [ ! -f $url ];then
		    value_err=1 
		   else
		    value_ok=0
		fi
	done
     fi
   done
}
makejson(){
	
   for stat in $value_err $value_ok;do
     if [ $stat -eq 0 ];then
       echo -n [{\"endpoint\": \"$endpoint\",\
         \"tags\": \"\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.vfcc.hdata.hotfile\",\
        \"value\": $stat, \
        \"counterType\": \"GAUGE\", \"step\": 60}]		
   else
     echo -n [{\"endpoint\": \"$endpoint\",\
         \"tags\": \"\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.vfcc.hdata.hotfile\",\
        \"value\": $stat, \
        \"counterType\": \"GAUGE\", \"step\": 60}]
   fi
done
}

##################################################################
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
vfcc=$(ps -ef|grep vfcc|grep nginx|wc -l)
endpoint=$(hostname -s)
timestamp=$(date +%s)

if [[ $vfcc -gt 0 ]]; then
        check_hotfile
	makejson
else
    exit;
fi
