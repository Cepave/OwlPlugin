#!/bin/bash
#===============================================================================
#   DESCRIPTION:vfcc squidclient info 信息输出! 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2017/02/12 
#===============================================================================

main() {
    nginx_status
    nginx_value 
    makejson
}
nginx_status() {

infofile="/tmp/.nginxinfo.owl.txt"
outinfo=$(curl 127.0.0.1/stub_status > $infofile 2>&1)

Active_conne=$(awk '/Active/ {print $3}' $infofile)

Reading_Writing=$(awk '/Reading/ {rwing=(($2+$4))} END {print rwing}' $infofile)

Waiting=$(awk '/Waiting/ {print $6}' $infofile)

}
nginx_value (){

nginx_value=($Active_conne $Reading_Writing $Waiting)

}
makejson(){
i=0
echo -n '['
CHAR=(',' ',' )

for info in Active_conne Reading_Writing Waiting 

do

cat << EOF
    {
        "endpoint"      :"$endpoint",
        "tags"          :"",
        "timestamp"     :$timestamp,
        "metric"        :"service.vfcc.ngx.$info",
        "value"         :${nginx_value[$i]},
        "counterType"   :"GAUGE",
        "step"          :60
    }${CHAR[$i]}
EOF
((i++))
done
echo -n ']'
}
#########################################################################
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
endpoint=$(hostname -s)
timestamp=$(date +%s)
vfcc=$(ps -ef|grep vfcc|grep nginx|wc -l)
if [ $vfcc -gt 0 ];then
	main
    else

	exit 0

fi
