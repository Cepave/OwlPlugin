#!/bin/bash
#=================================
#   DESCRIPTION:log监控 
#       设备组:vfcc 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2017/3/6 16:31
#2017 3/6 统计域名http 状态码，数据 
#03/10 update 过滤非法域名
#2017 4 17 切分日志分析
#2017 4 18 以百分比输出结果
#================================

#每分钟过滤vfcc 访问日志4xx或5xx的状态数据并排除到华数401 405防盗链，减少误报
Log(){

HEKA_FILE=/cache/logs/heka-data/$TODATA/$nowtime.log
TMP_FILE="/tmp/.owl_nginx_access.log"

if [ $LT -lt 5 ];then
   TIME=$(date -d "5  minutes ago" +%Y:%H:%M|sed 's/.$/\[0-4\]/')
else
   TIME=$(date -d "5  minutes ago" +%Y:%H:%M|sed 's/.$/\[5-9\]/')
fi

    sed -n "/$TIME/"p $HEKA_FILE > $TMP_FILE 2>&1

domain_tmp=$(ls /cache/logs/data_to_ftp/$TODATA/*|tail -1)
domain_all=$(awk '!a[$2]++ {print $2}' $domain_tmp|egrep -v 'yximgs|fastweb|qq')
SUM_CODE=$(wc -l $TMP_FILE|awk '{print $1}')

#忽略快手 快网 腾讯域名
}
check_vfcc_http404(){
echo -n "["

for domain in ${domain_all[@]};do 

      VF_SUM4=$(fgrep $domain $TMP_FILE |egrep -v '#_#|HEAD|favicon.ico|fwxgx.com|qq.com'|awk '$9~/40./ {print $9}'|egrep -vE '40[0-3]|405'|wc -l)

local domain_sum=$(grep -c $domain $TMP_FILE)
local value=$(echo "scale=3;$VF_SUM4 / $domain_sum * 100"|bc)
if [ -n $value ];then
     echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"domain=$domain\", \"timestamp\": $timestamp, \"metric\": \"service.vfcc.http.4xxcode\",\"value\": $VF_SUM4, \"counterType\": \"GAUGE\", \"step\": 300},
else
    echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"domain=$domain\", \"timestamp\": $timestamp, \"metric\": \"service.vfcc.http.4xxcode\",\"value\": 0, \"counterType\": \"GAUGE\", \"step\": 300},
fi
done
}
#每分钟过滤vfcc 5xx的状态数据
check_vfcc_http5xx(){
for domain in ${domain_all[@]};do 

      VF_SUM5=$(fgrep $domain $TMP_FILE |egrep -v '#_#|HEAD|favicon.ico|fwxgx.com|qq.com'|awk '$9~/50./ {print $9}'|wc -l)

local domain_sum=$(grep -c $domain $TMP_FILE)
local value=$(echo "scale=3;$VF_SUM5 / $domain_sum * 100"|bc)
if [ -n $value ];then
    echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"domain=$domain\", \"timestamp\": $timestamp, \"metric\": \"service.vfcc.http.5xxcode\",\"value\": $value, \"counterType\": \"GAUGE\", \"step\": 300},
else
    echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"domain=$domain\", \"timestamp\": $timestamp, \"metric\": \"service.vfcc.http.5xxcode\",\"value\": 0, \"counterType\": \"GAUGE\", \"step\": 300},
fi
done
}
check_http_code(){

   http_code=$(egrep -v '#_#|HEAD|favicon.ico|fwxgx.com|qq.com' $TMP_FILE|awk '{print $9}'|sort|uniq -c|sort -rn|awk '{print $2}'|grep [0-9])

for i in ${http_code[@]};do

      code_sum=$(egrep -v '#_#|HEAD|favicon.ico|fwxgx.com|qq.com' $TMP_FILE | awk '{print $9}'|sort|uniq -c|sort -rn|grep $i|awk '{print $1}'|grep [0-9])
local value=$(echo "scale=3;$code_sum  / $SUM_CODE * 100"|bc)

if [ -n $value ];then

   echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"code=$i\", \"timestamp\": $timestamp, \"metric\": \"service.vfcc.http.code\",\"value\": $value, \"counterType\": \"GAUGE\", \"step\": 300},
else
   echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"code=$i\", \"timestamp\": $timestamp, \"metric\": \"service.vfcc.http.code\",\"value\": 0, \"counterType\": \"GAUGE\", \"step\": 300},

fi
done|sed -e 's/,$/]/'
 
}

##################################################################
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
vfcc=$(ps -ef|grep vfcc|grep nginx|wc -l)
TODATA=$(date +%Y%m%d)
nowtime=$(date +%Y%m%d%H)
LT=$(date -d "5  minutes ago" +%Y:%H:%M|cut -c 10-10)

endpoint=$(hostname -s)
timestamp=$(date +%s)

if [[ $vfcc -gt 0 ]]; then
    Log
    check_vfcc_http404
    check_vfcc_http5xx
    check_http_code

else
    exit;
fi
