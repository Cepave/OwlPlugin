#!/bin/bash
#===============================================================================
#   DESCRIPTION:军网超父 回源网络监控 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/11/17 11:22
#增加军网下载速率监控
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
main() 
{
   httpingloss
   god_ip_alive
   speed_download
   httpingms
}

httpingloss()

{
    echo -n "["
    for IP in ${CHINAMILIP[@]}
    do
        value=$(httping $IP -c10 -t 2 -f|tail -n2|grep connects|awk '{print $5}'|sed -e 's/%/ /')

    echo -n {\"endpoint\": \"$endpoint\",\
    \"tags\": \"军网源loss=$IP\",\
    \"timestamp\": $timestamp,\
    \"metric\": \"service.net.httpingloss.vv\",\
    \"value\": $value, \
    \"counterType\": \"GAUGE\", \"step\": 120},
    done
}

god_ip_alive()

{
    for IP in ${CHINAMILIP[@]};do
        local value=$(httping $IP -N 1 -c10 -t2 -f > /dev/null;echo $?) 

    echo -n {\"endpoint\": \"$endpoint\",\
    \"tags\": \"upstreams_alive=$IP\",\
    \"timestamp\": $timestamp,\
    \"metric\": \"service.http.httping.alive.vv\",\
    \"value\": $value, \
    \"counterType\": \"GAUGE\", \"step\": 120}, 
   done
}

speed_download()

{
    for IP in ${CHINAMILIP[@]}
        do
    SPEED=$(/usr/bin/curl -s -o /dev/null -r 0-524288 -w '%{speed_download}' \
    'http://vv.chinamil.com.cn/asset/category3/2016/09/23/asset_252022.mp4' -x $IP:80  -m 3)
    value=$(echo "$SPEED" '*' "8" | bc)
    echo -n {\"endpoint\": \"$endpoint\",\
            \"tags\": \"军网源speed=$IP\",\
            \"timestamp\": $timestamp,\
            \"metric\": \"service.net.speed.vv\",\
            \"value\": $value, \
            \"counterType\": \"GAUGE\", \"step\": 120},
    done
}

httpingms()

{
    for IP in ${CHINAMILIP[@]}
    do
    local value=$(httping $IP  -c10 -t 2 -f|tail -n2|grep round-trip |awk -F "/" '{print $5}'|sed -e 's/ms//')

    owl="{\"endpoint\": \"$endpoint\",\
         \"tags\": \"军网源ms=$IP\",\
         \"timestamp\": $timestamp,\
         \"metric\": \"service.net.httpingms.vv\",\
         \"value\": $value, \
         \"counterType\": \"GAUGE\", \"step\": 120}," 
    done

   echo $owl|sed -e 's/,$/]/'
}
#####################################################
s01_p02_gd=$(/FastwebApp/fwutils/bin/fwhoami|egrep s01.p02_gd|wc -l)
if [ $s01_p02_gd -gt 0 ];then
CHINAMILIP=$(cat /opt/vfcc/nginx/conf/vhost.d/vv.chinamil.com.cn.conf |egrep fail_timeout|egrep -v "#"|awk '!a[$2]++ {print $2}'|sed -e 's/:80//')
endpoint=$(hostname -s)
timestamp=`date +%s`

   main

exit;
fi
