#!/bin/bash
#=================================
#   DESCRIPTION:log监控 
#       设备组:vfcc 
# 
#        AUTHOR: CATNOT
#       CREATED: 2016/9/6 16:31
#2017 2/7 增加vfcc diskManager 错误输出监控
#================================
#检查vfcc v1 版本logagent
check_vfcc_logagent_v1(){
       if [ -f /cache/logs/vfworker_log/http_log.log ];then
        local FILE=$(tail -n1 /cache/logs/*.error.log | awk -F ' |,' '/add2worker: \/file_/{print $0}'|tail -n1|awk '{print $18}'|awk -F "?" '{print $1}')
	local CHECK_FILE=$(grep -c "$FILE" /cache/logs/vfworker_log/http_log.log )
        return $CHECK_FILE
else
   exit 0
fi
}

#检查vfcc v2 版本logagent
check_vfcc_logagent_v2(){
        if [ -f /opt/vfcc2/sapper/sapper.py ];then
         local FILE=$(tail -n1 /cache/logs/vfcc2.error.log|awk '{print $18}'|awk -F "?" '{print $1}')
	 local CHECK_FILE=$(grep  -c "$FILE" /cache/logs/sapper/log/sapper.log)
         return $CHECK_FILE 
        else
          exit 0
        fi
}
check_vfcc_diskManager(){
	DISK_Manager=$(cat /opt/vfcc2/diskManager/nohup.out|wc -l)
	return $DISK_Manager
}
##################################################################
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
vfcc=`ps -ef|grep vfcc|grep nginx|wc -l`
#nowtime=`date +%Y:%H:%M`
endpoint=`hostname -s`
timestamp=`date +%s`

if [[ $vfcc -gt 0 ]]; then
    msg1=$(check_vfcc_logagent_v1)
    value=$?
    msg2=$(check_vfcc_logagent_v2)
    value2=$?
    msg3=$(check_vfcc_diskManager)
    value3=$?
else
    exit;

fi
    echo -n [{\"endpoint\": \"$endpoint\",\
         \"tags\": \"vfcc=v1\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.vfcc.logagent.alive\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 300},\
        {\"endpoint\": \"$endpoint\",\
         \"tags\": \"vfcc=v2\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.vfcc.logagent.alive\",\
        \"value\": $value2, \
        \"counterType\": \"GAUGE\", \"step\": 300},\
	 {\"endpoint\": \"$endpoint\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.vfcc.diskManager.err\",\
        \"value\": $value3, \
        \"counterType\": \"GAUGE\", \"step\": 300}]
