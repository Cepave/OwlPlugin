#!/bin/bash

#===============================================================================
#   DESCRIPTION:vfcc squid 内存 信息输出! 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2017/03/9 
#2017 03 30 新增squid cpu占用监控
#修改内存kb 转换 mb
#修正ps 取cpu平均值，top 取瞬时值
#===============================================================================

main(){
   squid_mem
   squid_cpu  
   makejson
  
}
squid_mem(){
    mem=$(ps -e -o 'comm,rsz,vsz' |grep squid|awk '{sum+=$2}END{print sum / 1024 }')

}
squid_cpu(){
    pcpu=$(top -bn 1|grep squid|sort -k9rn |head -1|awk '{print $9}')
}

makejson(){

cat << EOF
    [{
        "endpoint"      :"$endpoint",
        "tags"          :"",
        "timestamp"     :$timestamp,
        "metric"        :"service.vfcc.squid.mem.user",
        "value"         :$mem,
        "counterType"   :"GAUGE",
        "step"          :60
    },
    {
        "endpoint"      :"$endpoint",
        "tags"          :"",
        "timestamp"     :$timestamp,
        "metric"        :"service.vfcc.squid.cpu.user",
        "value"         :$pcpu,
        "counterType"   :"GAUGE",
        "step"          :60
    }]
EOF
}
############################

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

vfcc=$(ps -ef|grep vfcc|grep nginx|wc -l)
endpoint=$(hostname -s)
timestamp=$(date +%s)

if [[ $vfcc -gt 0 ]];then

  main 

 else

   exit;

fi
