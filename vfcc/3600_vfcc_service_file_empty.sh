#!/bin/bash
#=================================
#   DESCRIPTION:vfcc_v5空文件扫描 
#       设备组:vfcc_test
# 
#        AUTHOR: 韦启胜
#       CREATED: 2017/4/6 
#=================================

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

find_data () {
    sum_empty=$(find /*data* -type f -empty | grep -vw '/data' | grep -v "^."|wc -l)
    return $sum_empty
}
##############################################################
vfcc=$(ps -ef|grep vfcc|grep nginx|wc -l)
vfcc_test=$(/FastwebApp/fwutils/bin/fwhoami|grep gzvfcc_test|wc -l)
endpoint=$(hostname -s)
timestamp=$(date +%s)
if [ $vfcc -gt 0 -a $vfcc_test -eq 1 ]; then
    mgs=$(find_data)
    value=$?
else
    value=0
fi

cat << EOF
[
    {
        "endpoint"      :"$endpoint",
        "timestamp"     :$timestamp,
        "metric"        :"service.vfcc.file.empty",
        "value"         :$value,
        "counterType"   :"GAUGE",
        "step"          :3600
    }
]
EOF
