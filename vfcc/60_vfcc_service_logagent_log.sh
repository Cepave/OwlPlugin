#!/bin/bash

#===============================================================================
#   DESCRIPTION:vfcc logagent 日志监控! 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2017/03/28 
#2017/3/29增加vfcc2.error.log 判断，过滤无量设备
#2017 3 29 tail -1000 日志，减少grep 日志过大的，占用cpu时间 
#2017-04-05 增加针对vfcc2.error.log 路径做分析,过滤条件增加过滤HEAD信息 
#2017-4-17 移除vfcc2.error.log 时间判断条件，改用当out 带宽大于20m 时进行分析
#===============================================================================

#获取out 带宽值
chk_out_net(){
    Ethernet=$(ip route | awk '/default/ {print $5}')
    outfirst=$(awk '/'$Ethernet'/{print $10 }' /proc/net/dev)
      sleep 30 
    outend=$(awk '/'$Ethernet'/{print $10 }' /proc/net/dev)
    sumout=$(($outend-$outfirst))
if [ $sumout -gt 167772160 ];then
   logagent
else
   value=0
fi
}

logagent(){
    logagent_time=$(tail -1 /cache/logs/logagent/logagent.log|awk '{print $1,$2}'|cut -d ":" -f -2)
        timestamp_logagent=$(date -d "$logagent_time" +%s)

    system_time=$(date +%'Y-%m-%d %H:%M')
        timestamp_system=$(date -d "$system_time" +%s)

     sum_time=$(expr $timestamp_system - $timestamp_logagent)

    #判断最近10分钟是否正常打印日志
    if [ $sum_time -gt 600 ];then

        value=1
     else
        value=0
    fi
}

makejson(){
cat << EOF
    [{
        "endpoint"      :"$endpoint",
        "tags"          :"",
        "timestamp"     :$timestamp,
        "metric"        :"service.vfcc.logagent.log",
        "value"         :$value,
        "counterType"   :"GAUGE",
        "step"          :60
    }]
EOF
}
############################

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
chk_proc=$0
ps_proc=$(ps aux|grep -v grep|grep -c $chk_proc)

vfcc=$(ps -ef|grep vfcc|grep nginx|wc -l)
endpoint=$(hostname -s)
timestamp=$(date +%s)

if [ $vfcc -gt 0 -a $ps_proc -eq 2 ];then

chk_out_net
makejson

 else

   exit;

fi
