#!/bin/bash
#===============================================================================
#   DESCRIPTION:vfcc squidclient info 信息输出! 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2017/02/12 
#===============================================================================

main() {
    squidclient
    squid_info
    makejson
}
squidclient() {

infofile="/tmp/.squidinfo.owl.txt"
outinfo=$(/opt/vfcc2/squid/bin/squidclient -p 81 mgr:info > /tmp/.squidinfo.owl.txt 2>&1)

# Squid 
#Number of HTTP requests received客户端http请求数量
http_requests=$(grep 'Number of HTTP requests received:' $infofile | cut -d':' -f2| tr -d ' \t' )
#Number of csquidclients accessing cache使用proxy的客户端的数量
csquidclients=$(grep 'Number of clients accessing cache:' $infofile | cut -d':' -f2| tr -d ' \t' )
#Number of ICP messages received接受到的icp query数量
icp_received=$(grep 'Number of ICP messages received:' $infofile | cut -d':' -f2| tr -d ' \t' )
#Number of ICP messages sent发出icp query数量
icp_sent=$(grep 'Number of ICP messages sent:' $infofile | cut -d':' -f2| tr -d ' \t' )
 
req_fail_ratio=$(grep 'Request failure ratio:' $infofile | cut -d':' -f2| tr -d ' \t' )
#Average HTTP requests per minute since start每分钟http request的数量
avg_http_req_per_min=$(grep 'Average HTTP requests per minute since start:' $infofile | cut -d':' -f2| tr -d ' \t' )
#verage ICP messages per minute since start每分钟处理的icp数量 
avg_icp_msg_per_min=$(grep 'Average ICP messages per minute since start:' $infofile | cut -d':' -f2| tr -d ' \t')
#Cache Request命中率
request_hit_ratio=$(grep 'Request Hit Ratios:' $infofile | cut -d':' -f3|cut -d',' -f1|tr -d ' %' )
#Cache Byte命中率 Byte Hit Ratios
byte_hit_ratio=$(grep 'Byte Hit Ratios:' $infofile | cut -d':' -f3|cut -d',' -f1|tr -d ' %' )

request_mem_hit_ratio=$(grep 'Request Memory Hit Ratios:' $infofile | cut -d':' -f3|cut -d',' -f1|tr -d ' %' )

request_disk_hit_ratio=$(grep 'Request Disk Hit Ratios:' $infofile | cut -d':' -f3|cut -d',' -f1|tr -d ' %' )

servicetime_httpreq=$(grep 'HTTP Requests (All):' $infofile | cut -d':' -f2|tr -s ' '|awk '{print $1}' )

process_mem=$(grep 'Process Data Segment Size via sbrk' $infofile | cut -d':' -f2|awk '{print $1}' )

cpu_usage=$(grep 'CPU Usage:' $infofile | cut -d':' -f2|tr -d '%'|tr -d ' \t' )

#存放cache的磁盘使用量
cache_size_disk=$(grep 'Storage Swap size:' $infofile | cut -d':' -f2|awk '{print $1}' )

#存放cache的内存使用量
cache_size_mem=$(grep 'Storage Mem size:' $infofile | cut -d':' -f2|awk '{print $1}' )

mean_obj_size=$(grep 'Mean Object Size:' $infofile | cut -d':' -f2|awk '{print $1}' )

}
squid_info (){

squid_info=($http_requests $csquidclients $icp_received $icp_sent $req_fail_ratio $avg_http_req_per_min $avg_icp_msg_per_min $request_hit_ratio $byte_hit_ratio $request_mem_hit_ratio $request_disk_hit_ratio $servicetime_httpreq $process_mem $cpu_usage $cache_size_disk $cache_size_mem  $mean_obj_size)

}
makejson(){
i=0
echo -n '['
CHAR=(',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '')

for info in http_requests csquidclients icp_received icp_sent req_fail_ratio avg_http_req_per_min avg_icp_msg_per_min request_hit_ratio byte_hit_ratio request_mem_hit_ratio request_disk_hit_ratio servicetime_httpreq process_mem cpu_usage cache_size_disk cache_size_mem mean_obj_size 

do

cat << EOF
    {
        "endpoint"      :"$endpoint",
        "tags"          :"",
        "timestamp"     :$timestamp,
        "metric"        :"service.squid.$info",
        "value"         :${squid_info[$i]},
        "counterType"   :"GAUGE",
        "step"          :60
    }${CHAR[$i]}
EOF
((i++))
done
echo -n ']'
}
#########################################################################
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
endpoint=$(hostname -s)
timestamp=$(date +%s)
vfcc=$(ps -ef|grep vfcc|grep nginx|wc -l)
if [ $vfcc -gt 0 ];then
	main
    else

	exit 0

fi

