#!/bin/bash
#=================================
#   DESCRIPTION:vfcc redis 队列数据
#       设备组:vfcc 
# 
#        AUTHOR: CATNOT
#       CREATED: 2016/10/8 
# 1.01 添加19738端口队例数据;优化redis数据输出
# 更新diskmgr 上报所有节点
#===============================

REDIS_19736() {
echo -n "["
for QUEUE in ${QUEUES[@]}; do

    local LENGTH=$(/opt/vfcc2/redis/bin/redis-cli -p 19736 ZCARD $QUEUE|awk '{print $1}')
     echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"redis-cli=$QUEUE\", \"timestamp\": $timestamp, \"metric\": \"vfcc.service.redis.19736\",\
             \"value\": $LENGTH, \"counterType\": \"GAUGE\", \"step\": 120},
done 
REDIS_19738(){
for QUEUE in ${QUEUES[@]}; do

    local LENGTH=$(/opt/vfcc2/redis/bin/redis-cli -p 19738 ZCARD $QUEUE|awk '{print $1}')
     echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"redis-cli=$QUEUE\", \"timestamp\": $timestamp, \"metric\": \"vfcc.service.redis.19738\",\
             \"value\": $LENGTH, \"counterType\": \"GAUGE\", \"step\": 120}, >> /tmp/check_redis.tmp
done
}
    sed -ie 's/,$/]/' /tmp/check_redis.tmp
    cat /tmp/check_redis.tmp
    rm -f /tmp/check_redis.tmp
}
############################################################################
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

vfcc=$(ps -ef|grep vfcc|grep nginx|wc -l)
endpoint=`hostname -s`
timestamp=`date +%s`
QUEUES=(MEMCACHEDZSET SSDCACHEDZSET PREPAREZSET)

if [ $vfcc -gt 0 ];then
    REDIS_19736
    REDIS_19738
  else
    exit;
fi
