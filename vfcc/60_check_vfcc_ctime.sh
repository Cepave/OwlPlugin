#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_check_vfcc_ctime.sh
# Revision:     1.0
# Date:         2017/1/9
# Author:       胡亚
# Email:        huya@fastweb.com.cn
# Description:  监控/cache/logs/devops/fwsa2/purge_cache/目录下文件的ctime
#-------------------------------------------------------------------------------

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function check_vfcc_ctime()
{
local file_name=`ls /cache/logs/devops/fwsa2/purge_cache/ 2>/dev/null`

if [ -z "$file_name" ];then
        return 0
else
        local file_ctime=`stat -t /cache/logs/devops/fwsa2/purge_cache/*|awk '{print $14}'|sort|head -1`
        local now_time=`date +%s`
        local diff_t=`expr $now_time - $file_ctime`
        [ $diff_t -gt 900 ] && return 1  || return 0
fi

}
#---------------------------------------------------------------------------------

# Call function
msg=$(check_vfcc_ctime)
retval=$?
date=`date +%s`
host=`hostname -s`
tag=""

# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$host",
    "tags"       : "$tag",
    "timestamp"  : $date,
    "metric"     : "check.vfcc.ctime",
    "value"      : $retval,
    "counterType": "GAUGE",
    "step"       : 60
  }
]
EOF
