#!/bin/bash
#=================================
#   DESCRIPTION:多玩本地curl code监控 
#       设备组:vfcc 超父
# 
#        AUTHOR: CATNOT
#       CREATED: 2017/2/8 
#=================================

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

vfcc_super_code () {
	http_code=$(curl -I -so /dev/null -w '%{http_code}' http://w5.dwstatic.com/do_not_delete/t.mp4 -x 127.0.0.1:80 -m 3 --retry-delay 20 --retry 3)
}
##############################################################
endpoint=`hostname -s`
timestamp=`date +%s`
if [[ -f /var/named/dwstatic.com  ]]; then
	vfcc_super_code
 else
    exit;
fi
cat << EOF
[
    {
        "endpoint"      :"$endpoint",
        "timestamp"     :$timestamp,
        "metric"        :"service.http.code.yycom",
        "value"         :$http_code,
        "counterType"   :"GAUGE",
        "step"          :60
    }
]
EOF
