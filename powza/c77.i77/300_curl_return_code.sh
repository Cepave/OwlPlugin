#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     300_curl_return_code.sh
# Revision:     1.0
# Date:         2016/12/26
# Author:       胡亚
# Email:        huya@fastweb.com.cn
# Description:  c77.i77平台机器拉流的监控

#--------------------------------------------------------------------------------
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function curl_return_code()
{
local http_code=`curl -so /dev/null http://127.0.0.1:8000/live/flv/channel2 -m 10 -w '%{http_code}\n'`
if [ "$http_code" = "404" -o "$http_code" = "000" ];then
curl -so /dev/null http://127.0.0.1:8000/live/flv/channel2 -m 10 -w '%{http_code}\n'
else
echo $http_code
fi
}
#---------------------------------------------------------------------------------

# Call function
msg=$(curl_return_code)
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$host",
    "tags"       : "$tag",
    "timestamp"  : $date,
    "metric"     : "pzs.curl.return.code",
    "value"      : $msg,
    "counterType": "GAUGE",
    "step"       : 300
  }
]
EOF
