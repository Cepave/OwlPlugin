#!/bin/bash
########################################
#DESCRIPTION:lvs vip是否正常! 
#curl 请求url 是否200 
#AUTHOR:  CARNOT
#CREATED: 2016/06/14 16:31
#########################################

function check_fcd_lvs_vip {
  local VIP=`ip a|grep lo:0|awk '{print $4}'`
    if [  -n "$VIP" ]; then
        check_url=$(curl -so /dev/null -w '%{http_code}' http://fastweb.com.cn/do_not_delete_noc/100k.jpg -x $VIP:80 -m 3 --retry-delay 20 --retry 3)
      if [ $check_url -eq 302 ];then
          return 200
        else
          return $check_url
      fi
    else
       exit 200
    fi

}

function check_vfcc_lvs_vip
{
  local VIP=`ip a|grep lo:0|awk '{print $4}'`
    if [  -n "$VIP" ]; then
      check_url=$(/usr/bin/curl -s -o /dev/null -w '%{http_code}' http://update2.fastweb.cdn.qq.com/dltest/dltest.gif -x  $VIP:80 -m 3 --retry-delay 20 --retry 3)
    return $check_url 
    else
      exit 200
    fi
}
#########################################################################
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
vfcc=`ps -ef|grep vfcc|grep nginx|wc -l`

if [ $vfcc -gt 0 ]; then
        msg1=$(check_vfcc_lvs_vip)
   else
        msg2=$(check_fcd_lvs_vip)
fi

value=$?
endpoint=`hostname -s`
timestamp=`date +%s`

echo -n [{\"endpoint\": \"$endpoint\",\
         \"tags\": \"$VIP\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.http.lvs\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 60}]

