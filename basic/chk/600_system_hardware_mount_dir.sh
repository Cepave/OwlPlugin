#!/bin/bash
########################################
#DESCRIPTION:mount 目录是否有重复 
#df 检查dir 是否有重复
#AUTHOR:  韦启胜
#CREATED: 2017/1/1
#########################################

check_dir() {
    echo -n "["
    for i in $(df | grep -vE '^Filesystem|tmpfs|cdrom'|awk '{print $6}'|uniq -c|sed 's/[ \t]*//g');do

      dir_sum=$(echo $i |awk -F "/" '{print $1}') 
      if [[ $dir_sum -ne 1 ]]; then

        echo -n {\"endpoint\": \"$endpoint\",\
            \"tags\": \"dir=$(echo $i|sed 's/^.//g')\",\
            \"timestamp\": $timestamp,\
            \"metric\": \"system.hardware.mount.dir\",\
            \"value\": $dir_sum, \
            \"counterType\": \"GAUGE\", \"step\": 600},
      fi

  done|sed -e 's/,$/]/' 

}
#########################################################################
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
endpoint=$(hostname -s)
timestamp=$(date +%s)

check_dir

