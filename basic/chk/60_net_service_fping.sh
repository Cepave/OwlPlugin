#!/bin/bash
#===============================================================================
#   DESCRIPTION:fping www.taobao.com 丢包跟延时 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/06/14 16:31
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

[ ! -x  /usr/sbin/fping ] &&  yum install -y fping
        fping -A  -u -c 10 www.taobao.com  > /tmp/fpingtaibao.log 2>&1

        if [ -f /tmp/fpingtaibao.log ];then
                loss=`awk '{print $5}' /tmp/fpingtaibao.log |awk -F '/' '{print $3}'|cut -d "%" -f 1`
                ms=`awk -F '/' '{print $9}' /tmp/fpingtaibao.log`
                else 
                        exit;
        fi

value=$loss
value1=$ms
endpoint=`hostname -s`
timestamp=`date +%s`

echo -n [{\"endpoint\": \"$endpoint\",\
         \"tags\": \"service.net.fping=fpingloss\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.net.fping\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 60},\
        \
        {\"endpoint\": \"$endpoint\",\
         \"tags\": \"service.net.fping=fpingms\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.net.fping\",\
        \"value\": $value1, \
        \"counterType\": \"GAUGE\", \"step\": 60}]

