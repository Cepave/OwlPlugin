#!/bin/bash

# Metric value definition:
# 0: OK
# 1: Error

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function main() {
    tmp_file="/tmp/CREATE_BYOWL_DISK_FREE_MONITOR.tmp"
    msg=0
    date=`date +%s`
    host=$HOSTNAME
    tag=""

    echo -n "[" > $tmp_file

    df -m | grep -vE '^Filesystem|tmpfs|cdrom'  | sed 's/%//'|sed 's/\/dev\///'| awk '{ print $4,$5,$6 }' | while read line
    do
        msg=0
        read disk_free disk_pused partition <<< $line
        if [ "$disk_free" -lt 400000 ] && [ "$disk_pused" -gt 95 ] && [ "$partition" != "/hdata" ]; then
            msg=1
        else
            msg=0
        fi
        tag="partition=${partition}"

        echo -n "{\
            \"endpoint\"   : \"$host\",\
            \"tags\"       : \"$tag\",\
            \"timestamp\"  : $date,\
            \"metric\"     : \"disk.free.error\",\
            \"value\"      : $msg,\
            \"counterType\": \"GAUGE\",\
            \"step\"       : 60}," >>  $tmp_file

    done
   
    sed -ie 's/,$/]/' $tmp_file
    cat $tmp_file
    rm -f $tmp_file

}


main
