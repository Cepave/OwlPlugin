#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     900_mistiming.sh
# Revision:     v0.1
# Date:         2017/1/5
# Author:       王伟
# Email:        wangwei@fastweb.com.cn
# Metric value definition:
# 0 - 5 : OK
#   > 5 : Error
# -------------------------------------------------------------------------------
# Revision v0.1
# Description:	探测NTP是否正常，不正常retry 2次
# -------------------------------------------------------------------------------

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function chk_mistiming() {
    /bin/sleep "$[RANDOM%600]"
    
    local rdate_time=`/usr/bin/rdate  -p ntp1.cachecn.net  |head -1 | awk -F "]" '{print $2}'`
    if [ -z "$rdate_time" ] ; then
        local rdate_time=`/usr/bin/rdate  -p ntp1.cachecn.net  |head -1 | awk -F "]" '{print $2}'`
        [ -z "$rdate_time" ] && return
    fi
    local local_time=`date '+%s'`
    local server_time=`/bin/date '+%s' -d "$rdate_time"`
    local mistiming=`expr $local_time - $server_time`

    echo ${mistiming#-}
}


# Call function
MISTIMING=$(chk_mistiming)
DATE=`date +%s`
HOST=$HOSTNAME
TAG=""

# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$HOST",
    "tags"       : "$TAG",
    "timestamp"  : $DATE,
    "metric"     : "chk.mistiming",
    "value"      : $MISTIMING,
    "counterType": "GAUGE",
    "step"       : 60
  }
]
EOF
