#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_check_plugin.sh 
# Revision:     1.0
# Date:         2017/3/17
# Author:       严峰
# Email:        yanfeng@fastweb.com.cn
# Description:  监控owl插件目录是否存在
#-------------------------------------------------------------------------------

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function check_plugin(){
    local MyPath=/usr/local/owl/owl-agent-updater/falcon-agent/5.2.0/plugin/

    if [ ! -d $MyPath ];then
        return 1
    else
        local answer=`ls $MyPath|wc -w`
        [ $answer -gt 0 ] && return 0 || return 1
    fi
}

#---------------------------------------------------------------------------------

# Call function
msg=$(check_plugin)
retval=$?
date=`date +%s`
host=`hostname -s`
tag=""

# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$host",
    "tags"       : "$tag",
    "timestamp"  : $date,
    "metric"     : "check.plugin",
    "value"      : $retval,
    "counterType": "GAUGE",
    "step"       : 60
  }
]
EOF

