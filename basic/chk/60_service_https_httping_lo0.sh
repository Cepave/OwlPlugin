#!/bin/bash

#===============================================================================
#   DESCRIPTION: httping lo0 443可用性
# 
#        AUTHOR: 韦启胜
#       CREATED: 2017/04/22 
#===============================================================================

chk_listen (){

ss_https=$(ss -l|grep -c https)

if [ $ss_https -eq 1 ];then
   https_staus=$(httping 127.0.0.1:443 -N 1 -c10 -t2 -f > /dev/null;echo $?)
else
   https_staus=0
fi 
}

makejson(){

cat << EOF
    [{
        "endpoint"      :"$endpoint",
        "tags"          :"",
        "timestamp"     :$timestamp,
        "metric"        :"service.https.httping.lo0",
        "value"         :$https_staus,
        "counterType"   :"GAUGE",
        "step"          :60
    }]
EOF
}
############################

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

endpoint=$(hostname -s)
timestamp=$(date +%s)

    chk_listen
    makejson
