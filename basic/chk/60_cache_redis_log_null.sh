#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_cache_redis_log_null.sh
# Revision:     1.0
# Date:         2016/09/18
# Author:       王伟
# Email:        wangwei@fastweb.com.cn
# Description:  实现/cache/logs/noc/fwredis/redis.log文件是否存在
# -------------------------------------------------------------------------------
# Revision 1.0
# 实现/cache/logs/noc/fwredis/redis.log文件是否存在
# 
# 0:OK
# 1:Error
# -------------------------------------------------------------------------------

REDIS_LOG=/cache/logs/noc/fwredis/redis.log

function redis_log {
        if [ -f $REDIS_LOG ];then
                echo OK
                return 0
        else
                echo Error
				( service fwredis restart ) && ( service fwsa restart ) && ( service fwsa2 restart )
                return $?
        fi
}

msg=$(redis_log)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"redis.log.null\",\
  \"value\"      : $retval,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"
