#!/bin/bash

# Metric value definition:
# #socket

# Get TCP socket count.
#   Return an integer to zabbix.
function tcp_socket() {

    case $1 in
        total)
        ss -s | awk '/^TCP:/{gsub(/,/," ",$0); print $2}';;
        estab)
        ss -s | awk '/^TCP:/{gsub(/,/," ",$0); print $4}';;
        closed)
        ss -s | awk '/^TCP:/{gsub(/,/," ",$0); print $6}';;
        orphaned)
        ss -s | awk '/^TCP:/{gsub(/,/," ",$0); print $8}';;
        synrecv)
        ss -s | awk '/^TCP:/{gsub(/,/," ",$0); print $10}';;
        *)
        echo 0;;
    esac

}

# Call function
msgTotal=$(tcp_socket total)
msgEstab=$(tcp_socket estab)
msgClosed=$(tcp_socket closed)
msgOrphaned=$(tcp_socket orphaned)
msgSynrecv=$(tcp_socket synrecv)
retval=$?
date=`date +%s`
host=$HOSTNAME

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"type=total\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"net.tcp.socket\",\
    \"value\"      : $msgTotal,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60\
    },{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"type=estab\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"net.tcp.socket\",\
    \"value\"      : $msgEstab,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60\
    },{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"type=closed\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"net.tcp.socket\",\
    \"value\"      : $msgClosed,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60\
    },{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"type=orphaned\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"net.tcp.socket\",\
    \"value\"      : $msgOrphaned,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60\
    },{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"type=synrecv\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"net.tcp.socket\",\
    \"value\"      : $msgSynrecv,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60}]"
