#!/bin/bash
#===============================================================================
#   DESCRIPTION:fping www.taobao.com 丢包跟延时 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/06/14 16:31
#===============================================================================
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
function fpingloss
{
        local PFC=`fgrep -c FCDISK_PARENTBIG /usr/local/fastcache/etc/fastcache.conf`
	if [ $PFC -eq 1 -o -f /usr/local/fastcache/etc/fastcache.conf ];then
	echo -n "["
        for IP in ${SUPER[@]}
        do
	fping -A  -u -c 10 $IP  >> /tmp/fpingsuper.log 2>&1
        local value=`fgrep "$IP" /tmp/fpingsuper.log |awk '{print $5}'|awk -F '/' '{print $3}'|cut -d "%" -f 1`

	echo -n {\"endpoint\": \"$endpoint\",\
         \"tags\": \"fpingloss=$IP\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.net.cdn.fping\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 60},
        done
   else
        exit;
	fi
}
function fpingms
{
        local PFC=`fgrep -c FCDISK_PARENTBIG /usr/local/fastcache/etc/fastcache.conf`
	if [ $PFC -eq 1 -o -f /usr/local/fastcache/etc/fastcache.conf ];then
        for IP in ${SUPER[@]}
        do
        local value=`fgrep "$IP" /tmp/fpingsuper.log |awk -F '/' '{print $9}'`

        echo -n {\"endpoint\": \"$endpoint\",\
         \"tags\": \"fpingms=$IP\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.net.cdn.fping\",\
        \"value\": $value, \
        \"counterType\": \"GAUGE\", \"step\": 60}, >> /tmp/fpingsuper.tmp

        done
	sed -ie 's/,$/]/' /tmp/fpingsuper.tmp
	cat /tmp/fpingsuper.tmp
	rm -f /tmp/fpingsuper.tmp
   else
        exit;
fi

}

[ ! -x  /usr/sbin/fping ] &&  yum install -y fping
echo "" > /tmp/fpingsuper.log
SUPER=(111.1.43.115 115.238.138.42 222.186.142.25 101.71.84.42 101.71.84.43 111.1.43.106 221.181.168.97 222.186.129.135 115.238.138.44 101.71.84.44 112.5.69.150 115.238.138.43 115.238.138.56 115.238.138.55 222.186.142.24 221.204.224.75 36.250.78.150 111.1.43.114 221.204.224.76 111.1.43.105 222.88.91.51 221.204.224.74 222.186.142.26 111.1.43.104 101.71.84.55 101.71.84.56 110.80.134.150 122.192.111.135 61.54.31.51)
endpoint=`hostname -s`
timestamp=`date +%s`

fpingloss
fpingms
