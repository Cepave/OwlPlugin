#! /bin/bash

INTERVAL="5"
LOG="/dev/shm/lotServer-bandwidth.log"

date=`date +%s`
host=$HOSTNAME
BAND_SRV=0
BAND_NIC=0
BAND_WASTE=0
BAND_WASTERATE=0
DIFF_TIME=0

function push_metrics() {
    printf "[{\
      \"endpoint\"   : \"$host\",\
      \"tags\"       : \"\",\
      \"timestamp\"  : $date,\
      \"metric\"     : \"TCPLotSpeed.ServerOut.bits\",\
      \"value\"      : \"%.3f\",\
      \"counterType\": \"GAUGE\",\
      \"step\"       : 60\
      },{\
      \"endpoint\"   : \"$host\",\
      \"tags\"       : \"\",\
      \"timestamp\"  : $date,\
      \"metric\"     : \"TCPLotSpeed.AppExOut.bits\",\
      \"value\"      : \"%.3f\",\
      \"counterType\": \"GAUGE\",\
      \"step\"       : 60\
      },{\
      \"endpoint\"   : \"$host\",\
      \"tags\"       : \"\",\
      \"timestamp\"  : $date,\
      \"metric\"     : \"TCPLotSpeed.Waste.bits\",\
      \"value\"      : \"%.3f\",\
      \"counterType\": \"GAUGE\",\
      \"step\"       : 60\
      },{\
      \"endpoint\"   : \"$host\",\
      \"tags\"       : \"\",\
      \"timestamp\"  : $date,\
      \"metric\"     : \"TCPLotSpeed.WasteRate.percent\",\
      \"value\"      : \"%.3f\",\
      \"counterType\": \"GAUGE\",\
      \"step\"       : 60\
      },{\
      \"endpoint\"   : \"$host\",\
      \"tags\"       : \"\",\
      \"timestamp\"  : $date,\
      \"metric\"     : \"TCPLotSpeed.DiffTime.second\",\
      \"value\"      : \"%.3f\",\
      \"counterType\": \"GAUGE\",\
      \"step\"       : 60\
      }]" ${BAND_SRV} ${BAND_NIC} ${BAND_WASTE} ${BAND_WASTERATE} ${DIFF_TIME}
}

function get_bytes() {
	if [ ! -d /proc/net/appex ]; then
		push_metrics
		exit
	fi

	eval $(
		cat /proc/net/appex*/stats |\
		egrep 'LanInBytes|WanOutBytes' |\
		sed 's/=/ /g' |\
		awk '{array[$1] += $2} END {for(i in array) print i,array[i]}' |\
		sed 's/ /=/'
	)
}



if [ ! -s ${LOG} ]; then
	get_bytes
	echo "LASTTIME=$(date +%s)" > ${LOG}
	echo "LASTLANIN=${LanInBytes}" >> ${LOG}
	echo "LASTWANOUT=${WanOutBytes}" >> ${LOG}
	push_metrics
	exit
fi

get_bytes
if [ "$?" = "1" ]; then
	LanInBytes="0"
	WanOutBytes="0"
fi
CURTIME=$(date +%s)
CURLANIN=$LanInBytes
CURWANOUT=$WanOutBytes

. ${LOG}
DIFF_LANIN=$(echo "(${CURLANIN} - ${LASTLANIN})" | bc -l)
DIFF_WANOUT=$(echo "(${CURWANOUT} - ${LASTWANOUT})" | bc -l)
DIFF_TIME=$(echo "(${CURTIME} - ${LASTTIME})" | bc -l)

BAND_SRV=$(echo "${DIFF_LANIN} * 8 / ${DIFF_TIME}" | bc -l)
BAND_NIC=$(echo "${DIFF_WANOUT} * 8 / ${DIFF_TIME}" | bc -l)
BAND_WASTE=$(echo "(${DIFF_WANOUT} - ${DIFF_LANIN}) * 8 / ${DIFF_TIME}" | bc -l)
BAND_WASTERATE=$(echo "(${DIFF_WANOUT} - ${DIFF_LANIN}) * 100 / ${DIFF_LANIN}" | bc -l)

push_metrics

echo "LASTTIME=${CURTIME}" > ${LOG}
echo "LASTLANIN=${CURLANIN}" >> ${LOG}
echo "LASTWANOUT=${CURWANOUT}" >> ${LOG}
