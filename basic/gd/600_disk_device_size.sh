#!/bin/bash
# 2016-7-22 by jiangbin
# print all disk partition space size(KB)

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

endpoint=`hostname -s`
timestamp=`date +%s`

echo -n "["

#df -h | grep -vE '^Filesystem|tmpfs|cdrom'  | sed 's/%//'|sed 's/\/dev\///'| awk '{ print $5,"device="$1 }'| while read line
#df -hP | grep -vE '^Filesystem|tmpfs|cdrom'  | sed 's/%//'|sed 's/\/dev\///'| awk '{ print $5,"device="$1,"," }'|sed '$s/,//'| while read line
df -P | grep -vE '^Filesystem|tmpfs|cdrom'  | sed 's/%//'| awk '{split($1,jj,"/");print $2,"device="jj[length(jj)],"," }'|sed '$s/,//'| while read line
do 
read percent tag_name comma<<< ${line}
echo -n {\"endpoint\": \"$endpoint\", \"tags\": \"$tag_name\", \"timestamp\": $timestamp, \"metric\": \"disk.device.size\", \"value\": $percent, \"counterType\": \"GAUGE\", \"step\": 600}$comma
done

echo -n "]"

