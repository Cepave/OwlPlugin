#!/bin/bash


function pkg_installed() {
    local pkg_name=$1
    if (rpm -q $pkg_name); then
        return 0
    else
        return 1
    fi
}


# Call function

pkg_name="zabbix-agent"
msg=$(pkg_installed $pkg_name)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"pkg.installed\",\
    \"value\"      : $retval,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 600}]"

