#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_fwutils_fwhoami.sh
# Revision:     1.0
# Date:         2016/12/29
# Author:       胡亚
# Email:        huya@fastweb.com.cn
# Description:  判断/FastwebApp/fwutils/bin/fwhoami是否有成功返回

#--------------------------------------------------------------------------------
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function fwutils_fwhoami()
{
/FastwebApp/fwutils/bin/fwhoami &>/dev/null
if [ $? -eq 0 ] ;then
        return 0
else
        service fwredis restart &>/dev/null
        /FastwebApp/fwutils/bin/fwhoami &>/dev/null && return 0 || return 1
fi
}
#---------------------------------------------------------------------------------

# Call function
msg=$(fwutils_fwhoami)
retval=$?
date=`date +%s`
host=`hostname -s`
tag=""

# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$host",
    "tags"       : "$tag",
    "timestamp"  : $date,
    "metric"     : "fwutils.fwhoami",
    "value"      : $retval,
    "counterType": "GAUGE",
    "step"       : 60
  }
]
EOF
