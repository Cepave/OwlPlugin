#!/usr/bin/env perl
=comment;
2016-3-27 by JiangBin 
*********************************************************************************************************
实现功能:
采集网卡使用率

*********************************************************************************************************
v1.0.0 2016-6-24 采集网卡使用率
=cut;

#########################################################################################################
# PACKAGE

use strict;
use warnings;
use POSIX qw(strftime);

# PACKAGE
#########################################################################################################


#########################################################################################################
# CONSTANT

my $FILE_TIME_SEC = '/tmp/owl_netcard_time_sec.txt';
my $FILE_NETCARD_OUT_SIZE = '/tmp/owl_netcard_out_size.txt';
my $FILE_NETCARD_IN_SIZE = '/tmp/owl_netcard_in_size.txt';

# CONSTANT
#########################################################################################################


#########################################################################################################
# FUNCTION

# 取文件第一行的内容,用这个函数可以实现许多功能
sub Read_first_line {
    my $file = shift @_;
    open(my $fh,,$file) or return '';
    my $line = <$fh>;
    chomp $line;
    $line =~ s/^\s+|\s+$//g;
    return $line;
}

# 写入整个文件
sub Write_file {
    my ($file,$content) = @_;
    open(my $fh,">",$file) || die $!;
    select($fh);
    print("$content");
    select(STDOUT);
}

# FUNCTION
#########################################################################################################


############################################## MAIN START ###############################################
# MAIN

############
# 获取上次的时间
my $old_time_sec = Read_first_line($FILE_TIME_SEC);
# 获取本次的时间
my $now_time_sec = time();
chomp($old_time_sec,$now_time_sec);

# 保存本次的时间到文件.
Write_file($FILE_TIME_SEC,$now_time_sec);

# 如果上次时间没有则退出,等下次有的时候再输出
if (!$old_time_sec) {
    exit;
}

# 得出相差的时间秒数
my $diff_time_sec = $now_time_sec - $old_time_sec;


############
# 获取本机默认出口网卡名
my $default_netcard = readpipe("awk '\$2 == 00000000 { print \$1 }' /proc/net/route");
chomp($default_netcard);


############
# 获取上次的网卡out总输出量
my $old_netcard_out_size = Read_first_line($FILE_NETCARD_OUT_SIZE);

# 获取本次的网卡out总输出量
my $now_netcard_out_size = readpipe("cat /sys/class/net/$default_netcard/statistics/rx_bytes");
chomp($old_netcard_out_size,$now_netcard_out_size);

# 保存本次的网卡out总输出量
Write_file($FILE_NETCARD_OUT_SIZE,$now_netcard_out_size);

# 如果上次时间没有则退出,等下次有的时候再输出
if (!$old_netcard_out_size) {
    exit;
}

# 得出相差的out字节
my $diff_netcard_out_size = $now_netcard_out_size - $old_netcard_out_size;

############
# 获取上次的网卡in总输出量
my $old_netcard_in_size = Read_first_line($FILE_NETCARD_IN_SIZE);

# 获取本次的网卡in总输出量
my $now_netcard_in_size = readpipe("cat /sys/class/net/$default_netcard/statistics/tx_bytes");
chomp($old_netcard_in_size,$now_netcard_in_size);

# 保存本次的网卡in总输出量
Write_file($FILE_NETCARD_IN_SIZE,$now_netcard_in_size);

# 如果上次时间没有则退出,等下次有的时候再输出
if (!$old_netcard_in_size) {
    exit;
}

# 得出相差的out字节
my $diff_netcard_in_size = $now_netcard_in_size - $old_netcard_in_size;


############
# 获取默认出口网卡的总速率
my $speed = readpipe("cat /sys/class/net/$default_netcard/speed");
if (!$speed) {
    print "get netcard speed failed !";
    exit;
}

############

## 算出网卡最近60秒的平均使用率
#my $nic_in_usage_rate = sprintf "%.2f",$diff_netcard_out_size/($speed * 1048576) * 8 * 100;
#my $nic_out_usage_rate = sprintf "%.2f",$diff_netcard_in_size/($speed * 1048576) * 8 * 100;

# 算出网卡最近60秒的平均每1秒使用率
my $nic_in_usage_rate = sprintf "%.2f",$diff_netcard_out_size/$diff_time_sec/($speed * 1048576) * 8 * 100;
my $nic_out_usage_rate = sprintf "%.2f",$diff_netcard_in_size/$diff_time_sec/($speed * 1048576) * 8 * 100;

#print $speed;
#print "\n";
#print "$default_netcard out_usage_rate $diff_netcard_out_size = $nic_out_usage_rate ";
#print "\n";
#print "$default_netcard in_usage_rate $diff_netcard_in_size = $nic_in_usage_rate";
#print "\n";

# 输出json格式:
my $endpoint = readpipe("hostname -s");
chomp($endpoint);

print qq([{"endpoint": "$endpoint", "tags": "", "timestamp": $now_time_sec, "metric": "nic.out.usage.rate", "value": $nic_out_usage_rate, "counterType": "GAUGE", "step": 60},{"endpoint": "$endpoint", "tags": "", "timestamp": $now_time_sec, "metric": "nic.in.usage.rate", "value": $nic_in_usage_rate, "counterType": "GAUGE", "step": 60}]);



# MAIN
############################################### MAIN END ################################################
























