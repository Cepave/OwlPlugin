#!/bin/bash
# jiangbin
# 2016-6-22
# print nic card packets.

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

endpoint=`hostname -s`
defaul_out_card=`awk '$2 == 00000000 { print $1 }' /proc/net/route`
card_in_packets=`grep $defaul_out_card /proc/net/dev | awk '{print $3}'`
card_out_packets=`grep $defaul_out_card /proc/net/dev |awk '{print $11}'`
timestamp=`date +%s`
echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"nic.in.packets\", \"value\": \"$card_in_packets\", \"counterType\": \"GAUGE\", \"step\": 60},{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"nic.out.packets\", \"value\": \"$card_out_packets\", \"counterType\": \"GAUGE\", \"step\": 60}]

