#!/bin/bash

# Metric value definition:
# 0: OK
# 1: Error

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function nic_usage_rate_max() {
    local usage_rate=0
    for device in $(grep ":" /proc/net/dev | grep -vP '(bond|lo)' | awk '{sub(/:/," "); print $1}')
    do
        local link_speed=$(sudo /sbin/ethtool ${device} | awk '/Speed/{print $2}')
        local net_speed=`sar -n DEV |tail -n 20 | grep -v Average |grep $device |tail -n1 |awk '{printf "%d",$7*8/1000;exit}'`
        local nic_speed=${link_speed%%Mb/s}
        
        if [ ! -z $link_speed ]; then
            ( echo "$link_speed" | egrep -q '^[0-9]+' ) || continue
            ( echo "$net_speed" | egrep -q '^[0-9]+$' ) || continue

            local s_usage_rate=`echo "scale=2; $net_speed/$nic_speed*100" | bc | sed 's/...$//'`
            if [ "$s_usage_rate" -gt "$usage_rate" ]; then
                local usage_rate=$s_usage_rate
            fi
        fi
    done
    echo "$usage_rate"
    return 0
}



# Call function
msg=$(nic_usage_rate_max)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"nic.usage_rate.max\",\
    \"value\"      : $msg,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60}]"
