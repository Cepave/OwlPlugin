#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_rsync_daemon_error.sh
# Revision:     1.0
# Date:         2016/11/10
# Author:       胡亚
# Email:        huya@fastweb.com.cn
# Description:  采集本机镜像同步进程数量
#--------------------------------------------------------------------------------

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function rsync_daemon_error()
{
daemon_num=`ps -elf|grep "rsync --daemon"|egrep -v "grep|fwlog"|wc -l`
if [ $daemon_num -gt 5 ];then
     return 1
else
     return 0
fi
}
#---------------------------------------------------------------------------------

# Call function
msg=$(rsync_daemon_error)
retval=$?
date=`date +%s`
host=$HOSTNAME 
tag=""

# Send JSON message 
echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\ 
  \"metric\"     : \"rsync.daemon.error\",\
  \"value\"      : $retval,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"
