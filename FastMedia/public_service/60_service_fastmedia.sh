#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_service_fastmedia.sh 
# Revision:     1.0
# Date:         2017/3/3
# Author:       胡亚
# Email:        huya@fastweb.com.cn
# Description:  fastmedia下owl 的play publish pull push状态监控
#-------------------------------------------------------------------------------

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function service_fastmedia()
{
local file_name=$(ls /cache/logs/fmedia_stat_storeage/`date +%Y%m%d`|sort|tail -1)
cat /cache/logs/fmedia_stat_storeage/`date +%Y%m%d`/$file_name |grep -A 3 count_publish | sed -r 's/(.*)(>)([0-9]+)(<\/)(.*)/\3/'|awk -v num=$1 'NR==num{print}'
}
#---------------------------------------------------------------------------------

# Call function
msgplay=$(service_fastmedia 2)
msgpublish=$(service_fastmedia 1)
msgpull=$(service_fastmedia 3)
msgpush=$(service_fastmedia 4)
date=`date +%s`
host=`hostname -s`

# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$host",
    "tags"       : "status=play",
    "timestamp"  : $date,
    "metric"     : "service.fastmedia",
    "value"      : $msgplay,
    "counterType": "GAUGE",
    "step"       : 60
  },
  {
    "endpoint"   : "$host",
    "tags"       : "status=publish",
    "timestamp"  : $date,
    "metric"     : "service.fastmedia",
    "value"      : $msgpublish,
    "counterType": "GAUGE",
    "step"       : 60
  },
  {
    "endpoint"   : "$host",
    "tags"       : "status=pull",
    "timestamp"  : $date,
    "metric"     : "service.fastmedia",
    "value"      : $msgpull,
    "counterType": "GAUGE",
    "step"       : 60
  },
  {
    "endpoint"   : "$host",
    "tags"       : "status=push",
    "timestamp"  : $date,
    "metric"     : "service.fastmedia",
    "value"      : $msgpush,
    "counterType": "GAUGE",
    "step"       : 60
  }
]
EOF
