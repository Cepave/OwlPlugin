#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_check_heka_media.sh
# Revision:     1.0
# Date:         2016/10/12
# Author:       王伟
# Email:        wangwei@fastweb.com.cn
# Description:  实现fastmedia平台heka异常监控
# -------------------------------------------------------------------------------
# Revision 1.0
# 实现heka异常监控
#
# 0: OK
# 1: Error
# -------------------------------------------------------------------------------


PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function heka_process_alived {
    ps -ef | fgrep 'heka/etc/process_access.toml' | grep -v grep | wc -l
}

function heka_trans_check {
    if (sudo test ! -f /cache/logs/hekad/logstreamer/LogstreamerInput ); then
        echo Error
        return 1
    fi

    local heka_log_time_sec=`sudo grep -oP '[0-9]{12}(?=_acc\.log)' /cache/logs/hekad/logstreamer/LogstreamerInput`
    local heka_log_time=${heka_log_time_sec:0:10}
    local local_time=`date '+%Y%m%d%H'`
    local local_minute=`date '+%M'`

    if [ $heka_log_time != $local_time ] && [ $local_minute -gt 5 ]; then
        return 1
    else
        return 0
    fi
}

function heka_process_running {
    if [ $(heka_process_alived) -ne 1 ]; then
        echo Error
        initctl start heka
        return $?
    else
        echo OK
        return 0
    fi
}

function heka_trans_status {
    if ( ! heka_trans_check ); then
        echo Error
        return 1
    else
        echo OK
        return 0
    fi
}

# Call function
msg1=$(heka_trans_status)
retval1=$?
msg2=$(heka_process_running)
retval2=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"check.heka.media.status\",\
    \"value\"      : $retval1,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60},\
    {\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"check.heka.media.process.alived\",\
    \"value\"      : $retval2,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60}]"
