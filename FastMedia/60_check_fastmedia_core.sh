#!/bin/bash
#filename: check_fastmedia_core
#Athor: 季豪杰
#date:2016-9-23
#Revision:     1.0
#description: 监测 fastmedia 是否有core ;1为报警 0 为正常

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

version=$(curl -sI -m 15 http://127.0.0.1:8899/stat | grep Server | awk '{print $3}' | awk -F"\r" '{print $1}')
answer=$(ls -l /cache/logs/${version}/ | wc -l )

function check(){

    if [[ $answer -gt 1 ]];then
        return 1
    else
        return 0
    fi
}

ret=$(check)
retval1=$?
date=`date +%s`
host=$HOSTNAME
tag=""

echo -n "[{
    \"endpoint\"   : \"$host\",
    \"tags\"       : \"$tag\",
    \"timestamp\"  : $date,
    \"metric\"     : \"check.fastmedia.core\",
    \"value\"      : $retval1,
    \"counterType\": \"GAUGE\",
    \"step\"       : 60}]"
