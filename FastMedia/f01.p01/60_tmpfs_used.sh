#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_tmpfs_used.sh
# Revision:     1.0
# Date:         2016/12/30
# Author:       胡亚
# Email:        huya@fastweb.com.cn
# Description:  监控f01.p01  切片机 tmfps内存盘使用率
#-------------------------------------------------------------------------------

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function tmpfs_used()
{
	local use_tmpfs=`df -h |grep tmpfs|grep cache|awk '{printf "%d\n",$5}'`
	[ -n "$use_tmpfs" ] && echo $use_tmpfs || echo 2
}
#---------------------------------------------------------------------------------

# Call function
msg=$(tmpfs_used)
date=`date +%s`
host=`hostname -s`
tag=""

# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$host",
    "tags"       : "$tag",
    "timestamp"  : $date,
    "metric"     : "tmpfs.used",
    "value"      : $msg,
    "counterType": "GAUGE",
    "step"       : 60
  }
]
EOF
