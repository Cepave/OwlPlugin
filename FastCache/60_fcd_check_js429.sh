#!/bin/bash
#===============================================================================
#   DESCRIPTION:金山云源站429带宽保护监控；429时无法fcd父层将无法正常取源 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/06/14 16:31
#===============================================================================

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
accname=`date +%Y%m%d%H`_access.log
if [[ -f $accname ]];then

logrq=`date +%Y:%H:``date +%M|cut -c 1`
source=`cat /cache/logs/$accname |grep $logrq|awk '{ print $9 }'|grep 429|wc -l`

endpoint=`hostname -s`
value=$source
timestamp=`date +%s`
echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"service.http.c02p02\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 60}]
 else
exit
fi
