#!/bin/bash
# filename: 60_check_fcd_lru.sh
# Author: 季豪杰
# date:2016-9-21
# Revision:     1.0
# description: 监测 /etc/fastcache.conf store1.disk_size 是否达到 lru 条件

echo -n "["
number=$(cat /etc/fastcache.conf | grep "store1.disk_size" | awk '{print $3 }' | grep -o '[0-9]\{1,\}')
df -B 1g |  grep -P '/cache/cache\d+' | awk '{print $1,$3}' | while read line
do
used_value=$(echo $line | awk '{print $2}')
device=$(echo $line }| awk '{print $1}'| awk  -F '/' '{print $3}')
if [[ $used_value -gt $number ]];then
        return_value=1
else
        return_value=0
fi

echo 

#ret=$(fastcache_items)
retval1=$return_value
date=`date +%s`
host=$HOSTNAME
tag=$device

echo -n "{
    \"endpoint\"   : \"$host\",
    \"tags\"       : \"$tag\",
    \"timestamp\"  : $date,
    \"metric\"     : \"check.fcd.lru\",
    \"value\"      : $retval1,
    \"counterType\": \"GAUGE\",
    \"step\"       : 60} ",

done  | sed '$s/\,$//'

echo -n "]" 
