#!/bin/bash

# Metric value definition:
# 0: OK
# 1: Found core file

# Monitor core file in /cache/logs:
#   return the corefile name str to zabbix if found, otherwise return "OK".

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function oldworker_count() {

  ps aux |  grep -P 'fastcache.*old.*worker.*process' |grep -v grep | wc -l

}

msg=$(oldworker_count)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"fcd.oldworker.count\",\
  \"value\"      : $msg,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"
