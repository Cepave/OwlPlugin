#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_check_heka.sh
# Revision:     1.0
# Date:         2016/09/18
# Author:       王伟
# Email:        wangwei@fastweb.com.cn
# Description:  实现heka异常监控
# -------------------------------------------------------------------------------
# Revision 1.0
# 实现heka异常监控
#
# 0: OK
# 1: Error
# -------------------------------------------------------------------------------


PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH


function heka_input_alived {
    ps -ef | fgrep 'heka/etc/input_access.toml' | grep -v grep | wc -l
}

function heka_process_alived {
    ps -ef | fgrep 'heka/etc/process_access.toml' | grep -v grep | wc -l
}

function heka_trans_check {
	local log_file=`awk -F "," '{print $2 }' /cache/logs/hekad/logstreamer/LogstreamerInput |awk -F ":" '{print $2 }'|awk -F "\"" '{print $2 }'`
    	local log_seek=`awk -F "," '{print $1 }' /cache/logs/hekad/logstreamer/LogstreamerInput |awk -F ":" '{print $2 }'`
    	
	if [ -z "$log_file" ] || [ -z "$log_seek" ]; then
        	echo 0 && return 0
    	fi
    	
	local log_size=`ls -l $log_file |awk '{print $5}'`
    	local diff=$(($log_size-$log_seek))
	
	if [ $diff -gt 0 ] && [ $(date +%S) -gt 10 ]; then
        	local log_path=`grep "log_directory" /usr/local/heka/etc/process_access.toml |awk '{print $3}'|awk -F "\"" '{print $2}'`
        	local newer_file_size=`find $log_path -type f -newer $log_file | grep -v ".gz"| xargs wc -c |awk 'END{print $1}'`
        	local new_diff=$(($diff+$newer_file_size))
        	echo "$(($new_diff/1024/1024))"
        else
        	echo "$(($diff/1024/1024))"
    	fi
}

function heka_input_running {
    if [ $(heka_input_alived) -lt 1 -o $(heka_input_alived) -gt 5 ]; then
        return 1
    else
        return 0
    fi
}

function heka_process_running {
    if [ $(heka_process_alived) -ne 1 ]; then
        initctl start heka
        return $?
    else
        return 0
    fi
}

function heka_trans_status {
	if (test ! -f /cache/logs/hekad/logstreamer/LogstreamerInput ); then
            	local log_directory=`grep "log_directory" /usr/local/heka/etc/process_access.toml |awk '{print $3}'|awk -F "\"" '{print $2}'`
            	local log_file_count=`find $log_directory -type f |grep -v README|wc -l`
            	[ $log_file_count -gt 0 ] && echo 300 || echo 0
        else
		heka_trans_check
        fi
}

# Call function
msg1=$(heka_input_running)
retval1=$?
msg2=$(heka_trans_status)
msg3=$(heka_process_running)
retval3=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"check.heka.input.alived\",\
    \"value\"      : $retval1,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60},\
    {\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"check.heka.status\",\
    \"value\"      : $msg2,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60},\
    {\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"check.heka.process.alived\",\
    \"value\"      : $retval3,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60}]"
