#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_check_fcd_proc.sh
# Revision:     1.0
# Date:         2016/09/22
# Author:       王伟
# Email:        wangwei@fastweb.com.cn
# Description:  实现fcd master 和 cluster进程异常监控
# -------------------------------------------------------------------------------
# Revision 1.0
# 实现fcd master 和 cluster进程异常监控
#
# -------------------------------------------------------------------------------

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function proc_enabled {
    grep -q "^server.cluster = fastweb_cluster;$" /usr/local/fastcache/etc/fastcache.conf > /dev/null 2>&1
    return $?
}

function check_fcd_master {
    return `ps -ef | fgrep 'fastcache: master process' | grep -v grep | wc -l`    
}

function check_fcd_cluster {
    if proc_enabled;then
        return `ps -ef | fgrep 'fastcache: cluster manager process' | grep -v grep | wc -l`
    else
	echo no_alarm
        return 1
    fi
}

# Call function
msg1=$(check_fcd_master)
retval1=$?
msg2=$(check_fcd_cluster)
retval2=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"check.fcd.master.proc\",\
    \"value\"      : $retval1,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60},\
    {\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"check.fcd.cluster.proc\",\
    \"value\"      : $retval2,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60}]"
