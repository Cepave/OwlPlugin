#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_http_log_counter.sh
# Revision:     1.4
# Date:         2017/03/31
# Author:       王伟
# Email:        wangwei@fastweb.com.cn
# Description:  实现每5分钟到计数器日志抓取5xx超过总访问的20%报警
# -------------------------------------------------------------------------------
# Revision 1.0
# 实现每5分钟到计数器日志抓取5xx超过总访问的20%报警
#
# Revision 1.1
# 修正算术运算代码，解决BUG
#
# Revision 1.2
# 修改5xx报警阀值为20%
# 添加4xx报警代码
#
# Revision 1.3
# 添加五分钟内5xx最多报警域名
# 添加五分钟内4xx最多报警域名
#
# Revision 1.4
# 记忆tags数据，做画图校验使用（临时解决方案）
# -------------------------------------------------------------------------------

# Get Http Status Count.
#   Return an integer to zabbix.
function http_status_count() {

    # Arg Parse/Create Keyword:
    if [ "x${1}" = "x1XX" ] | [ "x${1}" = "x1xx" ] ; then
        local keyword="1001"
    elif [ "x${1}" = "x2XX" ] | [ "x${1}" = "x2xx" ] ; then
        local keyword="1002"
    elif [ "x${1}" = "x3XX" ] | [ "x${1}" = "x3xx" ] ; then
        local keyword="1003"
    elif [ "x${1}" = "x4XX" ] | [ "x${1}" = "x4xx" ] ; then
        local keyword="1004"
    elif [ "x${1}" = "x5XX" ] | [ "x${1}" = "x5xx" ] ; then
        local keyword="1005"
    else
        echo "-1"
        return 1
    fi

    # Ignore domain list:
    if [ "x${2}" != "x" ] ; then
        local ignore_domain=()
        while [ "x${2}" != "x" ] ; do
            ignore_domain=("${ignore_domain[@]}" "${2}")
            shift
        done
    fi

    # Time:
    local today=$(date +%Y%m%d)
    local tomorrow=$(date -d tomorrow +%Y%m%d%H%M)
    local hour=$(date +%H)

    local min_p1=$(date +%M | sed -e "s/\([0-9]\)[0-9]/\1/g")
    local min_p2=$(date +%M | sed -e "s/[0-9]\([0-9]\)/\1/g")
    if [ ${min_p2} -lt 5 ] ; then
        local min="${min_p1}0"
    else
        local min="${min_p1}5"
    fi

    # Logdir/LogFile:
    local logdir_squid="/cache/logs/data"
    local logdir_fastcache="/cache/logs/fcache_data"
    if [ -d "${logdir_fastcache}" ] ; then
        local logdir="${logdir_fastcache}"
        local log="${today}${hour}${min}.tmp"
    elif [ -d "${logdir_squid}" ] ; then
        local ts=$(echo "${today}${hour}${min}" | sed "s/\([0-9]\{4\}\)\([0-9]\{2\}\)\([0-9]\{2\}\)\([0-9]\{2\}\)\([0-9]\{2\}\)/\1-\2-\3 \4:\5/g")
        local logdir="${logdir_squid}"
        # Squid need delay 5 mins:
        local log="$(($(date +%s -d "${ts}") - 300)).tmp"
    else
        echo 0
        return 1
    fi

    if [ -f "${logdir}/${log}" ] ; then
        logfile="${logdir}/${log}"
    elif [ -f "${logdir}/${today}/${log}" ] ; then
        logfile="${logdir}/${today}/${log}"
    elif [ -f "${logdir}/${tomorrow}/${log}" ] ; then
        logfile="${logdir}/${tomorrow}/${log}"
    else
        echo 0
        return 1
    fi

    # Ignore Domain Set:
    if [ ${#ignore_domain[@]} -ne 0 ] ; then
        local ignore_domain_regex=$(echo ${ignore_domain[@]} | sed -e "s_ _\\\|_g")
    else
        local ignore_domain_regex="^$"
    fi

    local count=$(grep ".*\"${keyword} [0-9]* [0-9]*\".*$" "${logfile}" | \
                    fgrep -v '*' |\
                    grep -v "${ignore_domain_regex}" | \
                    sed -e "s/.*\"${keyword} \([0-9]*\) [0-9]*\".*$/\1/g" | \
                    awk 'BEGIN {sum = 0} {sum += $1} END {print sum}')    

    local max_5xx_domain=`cat ${logfile} | fgrep -v '*' |\
                          awk -F '"' '{for (i=1;i<=NF;i++){if ($i~/^1005 /){print $1,$i}}}' | \
                          awk '{a[$1" "$2]+=$4}END{for (i in a)print a[i],i}' | \
                          sort -nr | head -n1 | awk '{print $NF}'`
    
    local max_4xx_domain=`cat ${logfile} | fgrep -v '*' |\
                          awk -F '"' '{for (i=1;i<=NF;i++){if ($i~/^1004 /){print $1,$i}}}' | \
                          awk '{a[$1" "$2]+=$4}END{for (i in a)print a[i],i}' | \
                          sort -nr | head -n1 | awk '{print $NF}'`
    
    [ $count -gt 1433476929 ] && echo 0 || echo $count && echo $max_5xx_domain && echo $max_4xx_domain
    
    return 0
}

function alarm_5xx {
    local http_2xx=(`http_status_count 2xx kwmov.a.yximgs.com`)
    local http_3xx=(`http_status_count 3xx kwmov.a.yximgs.com`)
    local http_4xx=(`http_status_count 4xx kwmov.a.yximgs.com`)
    local http_5xx=(`http_status_count 5xx kwmov.a.yximgs.com`)

    # > 0.2 means more than 20%. +1 for prevent divide by zero.
    local percent_5xx_falot=$(echo "scale=2; ${http_5xx[0]} / (${http_2xx[0]} + ${http_3xx[0]} + ${http_4xx[0]} + ${http_5xx[0]} + 1)*100"|bc)
    local percent_5xx=`echo $percent_5xx_falot | awk '{printf("%d\n",$1)}'`

    # A and (B or C)
    if [ "${http_5xx[0]}" -gt "100" ] && ( [ "${http_2xx[0]}" -eq "0" ] || [ "${percent_5xx}" -gt "20" ] ); then
        echo Error 
        return 1
    else
        echo OK
        return 0
    fi
}

function alarm_4xx {
    local http_2xx=(`http_status_count 2xx kwmov.a.yximgs.com`)
    local http_3xx=(`http_status_count 3xx kwmov.a.yximgs.com`)
    local http_4xx=(`http_status_count 4xx kwmov.a.yximgs.com`)
    local http_5xx=(`http_status_count 5xx kwmov.a.yximgs.com`)

    # > 0.5 means more than 50%. +1 for prevent divide by zero.
    local percent_4xx_falot=$(echo "scale=2; ${http_4xx[0]} / (${http_2xx[0]} + ${http_3xx[0]} + ${http_4xx[0]} + ${http_5xx[0]} + 1)*100"|bc)
    local percent_4xx=`echo $percent_4xx_falot | awk '{printf("%d\n",$1)}'`

    # A and (B or C)
    if [ "${http_4xx[0]}" -gt "1000" ] && ( [ "${http_2xx[0]}" -eq "0" ] || [ "${percent_4xx}" -gt "50" ] ); then
        echo Error 
        return 1
    else
        echo OK
        return 0
    fi
}

function stateful_output() {
    local FILE_NAME=$1
    local METRIC_NAME=$2
    local ret_value=$3
    local TAG_NAME=$4
    local tag_value=$5

    local tmp_file=/tmp/$FILE_NAME.cumulated
    local target_file=/tmp/$FILE_NAME.once


    if [ $ret_value -eq 0 ]; then
        echo "$TAG_NAME=$tag_value  $METRIC_NAME  0   \n" >> $tmp_file
        target_file=$tmp_file
    else
        echo "$TAG_NAME=$tag_value  $METRIC_NAME  0   \n" >> $tmp_file
        echo "$TAG_NAME=$tag_value  $METRIC_NAME  $ret_value \n" >  $target_file
#       echo "$TAG_NAME=""          $METRIC_NAME  0.9        \n" >> $target_file
    fi

    s='{"endpoint":"%s","timestamp":%s,"tags":"%s","metric":"%s","value":%s,"counterType":"GAUGE","step":60}\n'
    rowCount=$(cat $target_file | wc -l)
    cat $target_file \
    | awk -v hostname="$(hostname -s)" -v date="$(date +%s)" '{print hostname, date, $0}' \
    | awk -v format=$s '{printf(format,$1,$2,$3,$4,$5) }' \
    | awk -v size=$rowCount 'BEGIN{print "["}{if(NR<size){print $0","} else {print $0}}END{print "]"}'

    # reset the cache
    if [ $ret_value -eq 0 ]; then
        cat /dev/null > $tmp_file
    fi
}

msg=$(alarm_5xx)
retval=$?
msg2=$(alarm_4xx)
retval2=$?
date=`date +%s`
host=$HOSTNAME
max_domain_5xx=(`http_status_count 5xx`)
max_domain_4xx=(`http_status_count 4xx`)
length=${#max_domain_4xx[@]}
tag1=${max_domain_5xx[1]}
if [ $length -eq 2 ];then
    tag2=${max_domain_4xx[1]}
else
    tag2=${max_domain_4xx[2]}
fi

stateful_output $(basename $0) http.log.error.5xx $retval Max_5xx_Domain $tag1
