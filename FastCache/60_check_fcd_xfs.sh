#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     60_check_fcd_xfs.sh 
# Revision:     1.0
# Date:         2017/3/22
# Author:       胡亚
# Email:        huya@fastweb.com.cn
# Description:  检查fcd xfs文件系统和存储分区
#-------------------------------------------------------------------------------

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function check_fcd_xfs(){
if [ -f /cache/logs/`date +%Y%m%d%H`_error.log ];then
        cat /cache/logs/`date +%Y%m%d%H`_error.log|egrep `date +%H:%M`|fgrep -q "Structure needs cleaning" &&return 1 ||return 0
else
        return 0
fi
}

function check_fcd_store(){
if [ -f /cache/logs/`date +%Y%m%d%H`_error.log ];then
        cat /cache/logs/`date +%Y%m%d%H`_error.log|egrep `date +%H:%M`|fgrep -q "Input/output error" &&return 1 ||return 0
else
        return 0
fi
}
#---------------------------------------------------------------------------------

# Call function
msg=$(check_fcd_xfs)
retval=$?
msg1=$(check_fcd_store)
retval1=$?
date=`date +%s`
host=`hostname -s`
tag=""

# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$host",
    "tags"       : "$tag",
    "timestamp"  : $date,
    "metric"     : "check.fcd.xfs",
    "value"      : $retval,
    "counterType": "GAUGE",
    "step"       : 60
  },
  {
    "endpoint"   : "$host",
    "tags"       : "$tag",
    "timestamp"  : $date,
    "metric"     : "check.fcd.store",
    "value"      : $retval1,
    "counterType": "GAUGE",
    "step"       : 60
  }
]
EOF

