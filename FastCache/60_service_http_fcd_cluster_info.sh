#!/bin/bash
#   DESCRIPTION:fastcache 本机Cache Hit 
#       设备组:fatcache 
# 
#        AUTHOR: 韦启胜
#       CREATED: 2016/12/12 
#修正计算数据重复
#增加3秒超时设置
#================================

sum_fastcache_cluster(){
    cluster=$(curl --connect-timeout 3 -si 127.0.0.1:8092 -m 3|egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'|awk '!a[$0]++ {print $0}'|wc -l)
    return $cluster
}
cache_hit_fatcache(){
    cache_hit=$(curl --connect-timeout 3 -si 127.0.0.1:8092 -m 3|egrep  '%</td>'|head -n1| egrep -o '[0-9]{1,3}\.[0-9]{1,3}')
}
##################################################################
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
fcd=$(pgrep fastcache|wc -l)
endpoint=$(hostname -s)
timestamp=$(date +%s)

if [[ $fcd -gt 0 ]]; then
    msg1=$(sum_fastcache_cluster)
    value1=$?
    cache_hit_fatcache
else
    exit;
fi
    echo -n [{\"endpoint\": \"$endpoint\",\
         \"tags\": \"\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.http.fcd.cluster.sum\",\
        \"value\": $value1, \
        \"counterType\": \"GAUGE\", \"step\": 60},\
        {\"endpoint\": \"$endpoint\",\
         \"tags\": \"\",\
        \"timestamp\": $timestamp,\
        \"metric\": \"service.http.fcd.cluster.hit\",\
        \"value\": $cache_hit, \
        \"counterType\": \"GAUGE\", \"step\": 60}]
