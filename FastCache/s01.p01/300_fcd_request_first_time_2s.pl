#!/usr/bin/env perl
=comment;
2016-11-22 by JiangBin 
*********************************************************************************************************
实现功能:
owl插件
王骏兴的需求:
1.每5分钟执行读取/cache/logs/%Y%m%d%H-access.log 最近5分钟的日志.
2.过滤 ".*/otv.*\.ts.*" （也就是URL包含otv和ts的URL）文件访问url
3.正则匹配某1个字段（FCACHE_MISS字段即可）
4.运算出他们的首包时间+下载时间是否>2秒  (FCACHE_MISS后有9个时间字段,只要是FCACHE_MISS的第5第6个时间就一定会有)
5.输出首包时间+下载时间>2秒的日志条数 (单位是毫秒)
这个脚本随便运行没事的,只是说他会从上次读取的地方开始读取,所以你要重头开读写的话要删除他的seek存档文件 /tmp/owl_fcd_request_first_time_gt_2s.seek
*********************************************************************************************************
v1.0.0 2016-11-22 提取首包时间加下载时间大于2秒的条目

=cut;

#########################################################################################################
# PACKAGE

use strict;
use warnings;
use POSIX qw(strftime);

# PACKAGE
#########################################################################################################

#########################################################################################################
# CONSTANT

# CONSTANT
#########################################################################################################

#########################################################################################################
# FUNCTION

# 获取seek日志里的位置
sub Read_seek_log {
    my ($file,$pre_5_min) = @_;
    # 如果seek_log文件或者域名total文件不存在,则创建一个,并返回0.
    # 如果seek_log文件为空,则输入0,并返回0.
    # 如果当前分钟是05分钟,就清空为0.
    #
    if ( (!-e $file) or ( -s $file eq 0 ) or ( $pre_5_min eq "00" ) ) {
        #qx(echo '0' \> $file);
        open (my $fh,">","$file") or die alert("error: can not open $file \n");
        select($fh);
        print "0";
        select(STDOUT);
        close $fh;
        return 0;
    }

    # 读取flag(获取上次的记录)
    open (my $fh,,"$file") or die alert("error: can not open $file \n");
    my $pre_size = <$fh>;
    chomp($pre_size);
    #$pre_size =~ s/^\s+|\s+$//g;
    close $fh;

    # 返回上次的保存值
    return $pre_size;
}

# 保存seek位置到日志里
sub Save_seek_log {
    my ($value,$file) = @_;
    # 保存x域名数据到记录文件
    open (my $fh,">","$file") or die alert("error: can not open $file \n");
    select($fh);
    print "$value";
    select(STDOUT);
    close $fh;
}

# FUNCTION
#########################################################################################################

############################################## MAIN START ###############################################
# MAIN

# fastcache的/cache/logs/%Y%m%d%H_access.log日志是每分钟第0秒会写入上一分钟的日志,并非实时更新的,所以每当分析这个日志x分钟时,先等待10秒
sleep 10;

# 获取本次的时间
my $now_time_sec = time();
chomp($now_time_sec);

# 累计数
my $count = 0;

# 2015-12-13 建立seek日志文件
my $SEEK_FILE = "/tmp/owl_fcd_request_first_time_gt_2s.seek";

# 2015-12-13 找出5分钟前的1小时日志文件,如果更改crontab间隔时间的话,这里的时间间隔也需要修改!!!!!!!!
my $fc_log_time_pre_5_min = readpipe("date -d'-5 min' '+%M'");
chomp($fc_log_time_pre_5_min);

my $previous_5_min = readpipe("date -d'-5 min' '+%Y%m%d%H'");
chomp($previous_5_min);
my $previous_5_min_log = "/cache/logs/".$previous_5_min."_access.log";

# 获取seek日志里的指针位置
my $seek_flag = Read_seek_log($SEEK_FILE,$fc_log_time_pre_5_min);

# 从seek位置读取所有日志
open(my $fh_read,,"$previous_5_min_log") || die alert("error: can not open $previous_5_min_log \n");
seek $fh_read,$seek_flag,0;
while( <$fh_read> ){
    if( $_ =~ /^(\S+) (\S+?) (\S+?) \[(\S+?)\/(\S+?)\/(\S+?)\:(\S+?:\S+?):(\S+?) (\S+?)\] "(\S+?) (http:\/\/)(\S+?)\/((\S*?)(\.ts+|\.otv+))((\S+?)?) (\S+?)" (\S+?) (\S+?) "(\S+?)" "((?#user agent)[^"]*)" FCACHE_MISS (\S+?) (\S+?) (\S+?) (\S+?) (\S+?) (\S+?) / ) {
        if ( ( $27 ne "-" ) && ( $28 ne "-" ) )  {
            my $first_time = $27 + $28;
            if ( $first_time > 2000 ) {
                $count++;
            }
        }
    }
}

# 获取最后读取的文件指针位置.
my $last_size = tell($fh_read);
#print " -------------  $last_size -----------\n";
close $fh_read;
Save_seek_log($last_size,$SEEK_FILE);

# 输出json格式:
my $endpoint = readpipe("hostname -s");
chomp($endpoint);
print qq([{"endpoint": "$endpoint", "tags": "", "timestamp": $now_time_sec, "metric": "fcd_request_first_time_gt_2s", "value": $count, "counterType": "GAUGE", "step": 300}]);

# MAIN
############################################### MAIN END ################################################
