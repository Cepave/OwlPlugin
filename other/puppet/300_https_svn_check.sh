#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:     300_https_svn_check.sh
# Revision:     v0.1
# Date:         2017/03/16
# Author:       王伟
# Email:        wangwei@fastweb.com.cn
# Metric value definition:
#   0: successfully allocate memory 
#   1: failure allocate memory 
# -------------------------------------------------------------------------------
# Revision v0.1
# Description:  五分钟删除10个配置文件或是20个证书文件，即触发报警
# -------------------------------------------------------------------------------
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function svn_check()
{
    local MYSVN=/data/mysvn
    cd $MYSVN && svn update > /tmp/https_svn_check
   
    SVN_DEL_STATUS=(`cat /tmp/https_svn_check | awk '{if($1=="D") print $2}' | \
            awk -F'/' '{print $NF}' | \
            awk -F'.' '{print $NF}' | \
            awk '{if($1=="autoconf")
                    {i++}
                  else if($1~/[cp][a-z]+/)
                    {j++}
                 }END{
                  print i?i:0,j?j:0}'`)
    echo ${SVN_DEL_STATUS[@]}
}

function alarm_https_svn()
{
    local HTTPS_SVN=(`svn_check`)
    local HTTPS_CONF=${HTTPS_SVN[0]}
    local HTTPS_CA=${HTTPS_SVN[1]}

    if [ "${HTTPS_CONF}" -gt 10 ] || [ "${HTTPS_CA}" -gt 20 ];then
        echo ERROR
        return 1
    else
        echo OK
        return 0
    fi
}

# Call function
MSG=$(alarm_https_svn)
RETVAL=$?
DATE=`date +%s`
HOST=$HOSTNAME
TAG=""

# Send JSON message
cat << EOF 
[
  {
    "endpoint"   : "$HOST",
    "tags"       : "$TAG",
    "timestamp"  : $DATE,
    "metric"     : "https.svn.check",
    "value"      : $RETVAL,
    "counterType": "GAUGE",
    "step"       : 300
  }
]
EOF
